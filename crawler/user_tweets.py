###
# CSC412 2013/2014 Semester 1 Assignment
# Group 7
###

import json
import os
import sqlite3
import sys
from tweepy import API, Cursor, OAuthHandler

from xml_tweet import get_xml_tweet

try:
    from config import *
except:
    pass



if __name__ == '__main__':
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = API(auth)

    screen_name = 'MintyTG'
    directory_name = '{0}_tweets'.format(screen_name)
    statuses = []
    count = 2823

    # Maximum of 3,200 tweets can be retrieved per user.
    # 200 results max per request, 3,200 / 200 = 16 pages.
    for page in Cursor(api.user_timeline, screen_name=screen_name, count=200).pages(16):
        for s in page:
            # Skip direct retweets as they are not written by the user
            if s.text[:2] != 'RT':
                statuses.append(s)

    # create directory for this user in the format of username_tweets
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)

    # output each tweet as an XML file
    for s in statuses:
        with open('{0}/{1:05d}.xml'.format(directory_name, count), 'w') as f:
            f.write(get_xml_tweet(s, count))
        count += 1

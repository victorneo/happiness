 # -*- coding: utf-8 -*-

import codecs
import sqlite3


if __name__ == '__main__':
    conn = sqlite3.connect('US.db')
    c = conn.cursor()
    f = codecs.open('us.csv', encoding='utf-8', mode='w+')

    for row in c.execute('SELECT tweet_id, screen_name, created_at, tweet  FROM tweets'):
        tweet = row[0].replace(',', ' ').replace('\n', ' ').strip()
        screen_name = row[1]
        f.write('%s,%s,%s,%s\n' % (row[3], screen_name, row[2], tweet))

    f.close()

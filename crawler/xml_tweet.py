# XML builder
from lxml import etree
from lxml.builder import E


def get_xml_tweet(s, count):
    """
    Returns the XML representation of a tweet as a String.
    """
    tweet = (
      E.doc( # creates an XML element called "doc"
        E.source('http://twitter.com/%s/status/%s' % (s.user.screen_name, s.id_str)),
        E.date(s.created_at.strftime('%Y.%m.%d')),
        E.text(s.text),
        {'id': str(count)}
      )
    )

    return etree.tostring(tweet, pretty_print=True)

###
# CSC412 2013/2014 Semester 1 Assignment
# Group 7
###

import json
import os
import sys

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

from xml_tweet import get_xml_tweet


try:
    from config import *
except:
    pass


class PublicTweetListener(StreamListener):
    """
    A listener handles tweets using Twitter's streaming API for public tweets.
    It exports each tweets to an XML file.
    """

    def __init__(self):
        super(PublicTweetListener, self).__init__()

        # Change count to start at the different number
        self.count = 7426
        self.end_count = 15000

        # All public tweets are placed in the public directory first
        self.directory_name = 'public'
        if not os.path.exists(self.directory_name):
            os.makedirs(self.directory_name)

    def on_status(self, status):
        with open('{0}/{1:05d}.xml'.format(self.directory_name, self.count), 'w') as f:
            f.write(get_xml_tweet(status, self.count))

        if self.count % 1000 == 0:
            print self.count

        self.count += 1

        if self.count == self.end_count:
            return False

        return True

    def on_error(self, status):
        print status


if __name__ == '__main__':
    l = PublicTweetListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # Lat Lon bounding boxes for three countries
    locations = [-124.848974,24.396308, -66.885444, 49.384358, # US
                 -9.23, 2.69, 60.85, 49.84, # UK
                 102,1.159444,104.4075,4] # SG

    # Request for english language tweets only
    languages = ['en',]

    # Start listening for public tweets
    stream = Stream(auth, l)
    stream.filter(locations=locations, languages=languages)

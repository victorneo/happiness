import codecs
import os
import subprocess
import sys
import re
from lxml import etree
from nltk import word_tokenize


CORENLP_JAR = 'libs/stanford-corenlp-3.2.0.jar'
DEFAULT_MODEL_FILE = 'data/pos_models/initial.tagger'
MODEL_FILE = 'data/pos_models/all.tagger'
TRAIN_FILE = 'data/pos_training/train_all.txt'
TEST_DIR = 'data/pos_testing/'
TAGGED_FILE = 'data/pos_tagged/output.txt'
TWEETS_DIR = 'data/tweets/'

TEMP_FILE = 'tweets.tmp.txt'
TEMP_OUTPUT_FILE = 'tweets_output.tmp.txt'

TESTING_START_COUNT = 7425
TESTING_END_COUNT = 14999 + 1


def tag_tweets(tweets, model=MODEL_FILE):
    """
    A very thin-wrapper over Stanford's POS tagger.
    Tag a list of tokenized tweets using the model from MODEL_FILE by default,
    unless specified otherwise by the user.
    """

    # Write each tweet out in a text file
    regex = re.compile('[\r\n]')
    with codecs.open(TEMP_FILE, 'w', encoding='utf-8') as tweets_file:
        for tweet in tweets:
            tweets_file.write(u' '.join(word_tokenize(regex.sub(' ', tweet))))
            tweets_file.write('\n')

    # Tag the tweets
    subprocess.call(['java', '-mx1g', '-cp', CORENLP_JAR,
                     'edu.stanford.nlp.tagger.maxent.MaxentTagger',
                     '-tagSeparator', '_',
                     '-model', model,
                     '-tokenize', 'false',
                     '-textFile', TEMP_FILE,
                     '-outputFile', TEMP_OUTPUT_FILE,])

    # Each tweet is now a list of tuples containing (word, POS tag)
    tagged_tweets = []
    with codecs.open(TEMP_OUTPUT_FILE, 'r', encoding='utf-8') as tweets_file:
        for l in tweets_file:
            tweet = []
            tagged_tokens = l.strip().split(' ')
            for t in tagged_tokens:
                parts = t.split('_')
                tweet.append((parts[0], parts[1]))

            tagged_tweets.append(tweet)

    # Clean up
    os.remove(TEMP_FILE)
    os.remove(TEMP_OUTPUT_FILE)

    return tagged_tweets


def tag():
    """
    Use Python's subprocess module to train and perform POS tagging using
    Stanford's MaxentTagger.
    """

    # Check that stanford-corenlp-3.2.0.jar and training file exists
    if not os.path.exists(CORENLP_JAR):
        print 'Ensure that libs/stanford-corenlp-3.2.0.jar exists'
        sys.exit(1)
    elif not os.path.exists(TRAIN_FILE):
        print 'Ensure that data/pos_training/train_all.txt exists'
        sys.exit(1)

    # Read in test set XML files and store them as a list
    regex = re.compile('[\r\n]')

    test_file = codecs.open(TEMP_FILE, 'w', encoding='utf-8')
    for i in range(TESTING_START_COUNT, TESTING_END_COUNT):
        tree = etree.parse('{0:s}{1:05d}.xml'.format(TWEETS_DIR, i))
        test_file.write(regex.sub(' ', tree.find('text').text.strip()))
        test_file.write('\n')
    test_file.close()

    # Train the tagger using tagged training data
    #subprocess.call(['java', '-mx1g', '-cp', CORENLP_JAR,
    #                 'edu.stanford.nlp.tagger.maxent.MaxentTagger',
    #                 '-arch', 'generic',
    #                 '-tagSeparator', '_',
    #                 '-trainFile', TRAIN_FILE,
    #                 '-model', MODEL_FILE])

    # Now we tag all test files
    subprocess.call(['javac', '-cp', CORENLP_JAR, 'PosTag.java'])
    subprocess.call(['java', '-mx1g', '-cp', CORENLP_JAR+':.',
                     'PosTag', MODEL_FILE, TEMP_FILE, TAGGED_FILE])

    # cleanup
    os.remove(TEMP_FILE)


if __name__ == '__main__':
    tag()

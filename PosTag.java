import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import java.io.*;


public class PosTag {
    public static void main(String [] args) throws IOException{
        MaxentTagger tagger = new MaxentTagger(args[0]);
        BufferedReader in = new BufferedReader(new FileReader(args[1]));
        PrintWriter out = new PrintWriter(args[2]);

        String line;
        String taggedString;
        while ((line = in.readLine()) != null){
            taggedString = tagger.tagString(line);
            out.println(taggedString);
        }
        out.close();
    }
}

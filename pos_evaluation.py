from collections import defaultdict
import codecs
import os
import subprocess
import sys
import re


CORENLP_JAR = 'libs/stanford-corenlp-3.2.0.jar'
MODEL_FILE = 'data/pos_models/train_9.tagger'
TRAIN_FILE = 'data/pos_training/train_all.txt'
TEST_FILE = 'data/pos_testing/test_all.txt'
TAGGED_FILE = 'data/pos_testing/test_all_tagged.txt'
TWEETS_DIR = 'data/tweets/testing/'


def evaluate():
    """
    Uses Python's subprocess module to evaluate POS tagger
    performance using 10-fold cross validation
    """

    # Check that stanford-corenlp-3.2.0.jar and training file exists
    if not os.path.exists(CORENLP_JAR):
        print 'Ensure that libs/stanford-corenlp-3.2.0.jar exists'
        sys.exit(1)

    # Read in test set XML files and store them as a list
    #tweets = []
    #regex = re.compile('[\r\n]')
    #for fname in os.listdir(TWEETS_DIR):
    #    tree = etree.parse('%s%s' % (TWEETS_DIR, fname))
    #    tweets.append(regex.sub(' ', tree.findall('text')[0].text))

    # Write out each tweet on a newline for tagger to read
    #with codecs.open(TEST_FILE, 'w', encoding='utf-8') as test_file:
    #    test_file.write('\r\n'.join(tweets))

    # Train the tagger using tagged training data
    #subprocess.call(['java', '-mx1g', '-cp', CORENLP_JAR,
    #                 'edu.stanford.nlp.tagger.maxent.MaxentTagger',
    #                 '-arch', 'generic',
    #                 '-tagSeparator', '_',
    #                 '-trainFile', TRAIN_FILE,
    #                 '-model', MODEL_FILE])

    # Check the output to measure confusion matrix
    tag_counts = defaultdict(int)
    wrong_counts = defaultdict(int)
    correct_counts = defaultdict(int)
    tagged_counts = defaultdict(int)
    all_tokens = []

    for l in codecs.open('data/pos_testing/test_10.txt', 'r', encoding='utf-8'):
        tokens = l.split(' ')
        line_tokens = []
        for t in tokens:
            tag = t.split('_')[-1].strip()
            tag_counts[tag] += 1
            line_tokens.append(tag)
        all_tokens.append(line_tokens)

    print len(all_tokens)

    output = subprocess.check_output(['java', '-mx1g', '-cp', CORENLP_JAR,
                             'edu.stanford.nlp.tagger.maxent.MaxentTagger',
                             '-tagSeparator', '_',
                             '-model', MODEL_FILE,
                             '-testFile', 'data/pos_testing/test_10.txt',
                             '-outputFile', 'data/pos_testing/test_10_results.txt'],)
                             #stderr=subprocess.STDOUT,)

    line_counter = 0
    token_counter = 0

    for l in output.split('\n'):
        if not l.strip():
            break
        tokens = l.split(' ')
        token_counter = 0
        for t in tokens:
            items = t.split('_')
            tagged = items[-1]
            tagged_counts[tagged] += 1
            if tagged == all_tokens[line_counter][token_counter]:
                correct_counts[tagged] += 1

            token_counter += 1
        line_counter += 1

    #for l in output.split('\n'):
    #    if 'Word:' in l:
    #        items = l.split(';')
    #        actual_class = items[1].split(':')[1].strip()
    #        wrong_counts[actual_class] += 1

    #print wrong_counts

    #for t in tag_counts.keys():
    #    if t in wrong_counts:
    #        print t, (float(tag_counts[t]) - wrong_counts[t]) / tag_counts[t]

    for k in tag_counts.keys():
        if correct_counts[k] == 0:
            precision = 0.0
            recall = 0.0
        else:
            precision = float(correct_counts[k]) / tagged_counts[k]
            recall = float(correct_counts[k]) / tag_counts[k]

        print k, ': Recall - ', '%.2f' % recall, ' Precision - ', '%.2f' % precision


if __name__ == '__main__':
    evaluate()

import codecs
import os
import subprocess
import sys
import re
from lxml import etree
from nltk.tree import Tree


CORENLP_JAR = 'libs/stanford-corenlp-3.2.0.jar'
MODEL_FILE = 'data/parser_models/final.dat'
TRAIN_DIR = 'data/parser_training'
TRAIN_START = 1
TRAIN_END = 509 + 1
PARSED_FILE = 'data/parsed/parsed.txt'
TWEETS_DIR = 'data/tweets'
TEMP_FILE = 'parser.tmp.txt'
TEMP_OUTPUT_FILE = 'parser.tmp.txt.stp'

TESTING_START_COUNT = 7425
TESTING_END_COUNT = 14999 + 1


def parse_tweets(tweets):
    trees = []
    for tweet in tweets:
        with codecs.open('parser.tmp.txt', 'w', encoding='utf-8') as f:
            f.write(tweet)

        subprocess.call(['java', '-mx512m', '-cp', CORENLP_JAR,
                         'edu.stanford.nlp.parser.lexparser.LexicalizedParser', '-writeOutputFiles',
                         MODEL_FILE, 'parser.tmp.txt'])

        with codecs.open('parser.tmp.txt.stp', 'r', encoding='utf-8') as f:
            trees.append(Tree('(' + f.read() + ')'))

    os.remove(TEMP_FILE)
    os.remove(TEMP_OUTPUT_FILE)

    return trees


def parse():
    """
    Use Python's subprocess module to train and perform Synthetic parsing using
    Stanford's LexicalParser.
    """

    # Check that stanford-corenlp-3.2.0.jar and training file exists
    if not os.path.exists(CORENLP_JAR):
        print 'Ensure that libs/stanford-corenlp-3.2.0.jar exists'
        sys.exit(1)
    elif not os.path.exists(TRAIN_DIR):
        print 'Ensure that data/parser_training exists'
        sys.exit(1)

    # Train the parser using training data
    #subprocess.call(['java', '-mx1500m', '-cp', CORENLP_JAR,
    #                 'edu.stanford.nlp.parser.lexparser.LexicalizedParser',
    #                 '-train', TRAIN_DIR, '%d-%d' % (TRAIN_START, TRAIN_END),
    #                 '-saveToSerializedFile', MODEL_FILE])

    # Output each tweet from testing set as a text file
    regex = re.compile('[\r\n]')
    test_file = codecs.open(TEMP_FILE, 'w', encoding='utf-8')
    #for i in range(TESTING_START_COUNT, TESTING_END_COUNT):
    for i in range(1, 101):
        #tree = etree.parse('{0:s}/{1:05d}.xml'.format(TWEETS_DIR, i))
        tree = etree.parse('data/MintyTG_tweets/{0:05d}.xml'.format(i))
        test_file.write(regex.sub(' ', tree.find('text').text.strip()))
        test_file.write('\n')
    test_file.close()

    # Now we parse all test files
    subprocess.call(['javac', '-cp', CORENLP_JAR, 'Parse.java'])
    subprocess.call(['java', '-mx1g', '-cp', CORENLP_JAR+':.',
                     'Parse', MODEL_FILE, TEMP_FILE, 'data/MintyTG_tweets/parsed.txt'])
                     #'Parse', MODEL_FILE, TEMP_FILE, PARSED_FILE])

if __name__ == '__main__':
    parse()

import codecs
import pickle
from flask import Flask, render_template, Response, request
from lxml import etree
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
from sklearn.externals import joblib


app = Flask(__name__)
app.debug = True
POS_FEATURES = {'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS', 'SYM', 'UH'}
PARSED_FEATURES = {'ADJP', 'NP',}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/tweets/', methods=['POST',])
def user():
    username = request.form['username']
    # Due to Twitter API restrictions, only show for users we have preloaded
    if username.lower() != 'mintytg':
        return render_template('index.html', error=True)

    # Load pre-tagged and parsed tweets
    tweets = []
    tagged_tweets = []
    tags = []
    parsed_tweets = []
    features = []

    for i in range(1, 101):
        tweets.append(etree.parse('MintyTG_tweets/{0:05d}.xml'.format(i)).find('text').text.strip())

    with codecs.open('MintyTG_tweets/tagged.txt', 'r', encoding='utf-8') as f:
        tagged_tweets = [l.strip() for l in f]

    with codecs.open('MintyTg_tweets/parsed.txt', 'r', encoding='utf-8') as f:
        tree_str = ''
        for l in f:
            l = l.strip()
            if l and not tree_str:
                tree_str = l
            elif l:
                tree_str += l
            elif not l and tree_str:
                parsed_tweets.append(Tree(tree_str))
                tree_str = ''

    if tree_str:
        parsed_tweets.append(Tree(tree_str))

    for (i, l) in enumerate(tagged_tweets):
        tokens = l.split()
        feature = set()
        tree = parsed_tweets[i]
        tags.append((tokens[0], tokens[1]))

        for st in tree.subtrees():
            if st.node in PARSED_FEATURES:
                for sst in st.subtrees():
                    if sst.node in POS_FEATURES:
                        for leaf in sst.leaves():
                            feature.add(leaf)

        for token in tokens:
            t = token.split('_')
            if t[1] in POS_FEATURES:
                feature.add(t[0])

        features.append(feature)

    # Load final trained classifier model
    clf = joblib.load('classifier/clf.pkl')
    dictionary = pickle.load(open('classifier/dictionary.dat', 'r'))
    tfidf = pickle.load(open('classifier/tfidf.dat', 'r'))

    tfidf_test_features = []
    for tf in features:
        vect = tfidf[dictionary.doc2bow([f.lower() for f in tf])]
        vect_dictionary = {v[0]: v[1] for v in vect}
        vect_tfidf = []
        for cnf in range(0, 2778):
            if cnf in vect_dictionary:
                vect_tfidf.append(vect_dictionary[cnf])
            else:
                vect_tfidf.append(0.0)
        tfidf_test_features.append(vect_tfidf)

    predictions = clf.predict(tfidf_test_features)
    happy_tweets = []
    sad_tweets = []
    for (i, p) in enumerate(predictions):
        if p == 1:
            NEs = []
            for st in ne_chunk(pos_tag(word_tokenize(tweets[i])), binary=True).subtrees():
                if st.node == 'NE':
                    NEs = [l[0] for l in st.leaves()]

            happy_tweets.append((tweets[i], ' '.join(NEs)))
        elif p == -1:
            NEs = []
            for st in ne_chunk(pos_tag(word_tokenize(tweets[i])), binary=True).subtrees():
                if st.node == 'NE':
                    NEs = [l[0] for l in st.leaves()]

            sad_tweets.append((tweets[i], ' '.join(NEs)))

    return render_template('tweets.html', username=username,
                            happy_tweets=happy_tweets, sad_tweets=sad_tweets)


if __name__ == '__main__':
    app.run()

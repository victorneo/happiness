
Cost: 0.10
	Fold 0
	0 Precision: 0.54 Recall: 0.79 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.65 Recall: 0.52 F1: 0.58 Accuracy: 0.69
	-1 Precision: 0.65 Recall: 0.42 F1: 0.51 Accuracy: 0.72
	Fold 1
	0 Precision: 0.53 Recall: 0.85 F1: 0.66 Accuracy: 0.65
	1 Precision: 0.71 Recall: 0.54 F1: 0.62 Accuracy: 0.75
	-1 Precision: 0.72 Recall: 0.41 F1: 0.52 Accuracy: 0.71
	Fold 2
	0 Precision: 0.55 Recall: 0.73 F1: 0.62 Accuracy: 0.61
	1 Precision: 0.57 Recall: 0.49 F1: 0.53 Accuracy: 0.69
	-1 Precision: 0.59 Recall: 0.39 F1: 0.47 Accuracy: 0.67
	Fold 3
	0 Precision: 0.55 Recall: 0.78 F1: 0.65 Accuracy: 0.63
	1 Precision: 0.68 Recall: 0.49 F1: 0.57 Accuracy: 0.69
	-1 Precision: 0.44 Recall: 0.32 F1: 0.37 Accuracy: 0.66
	Fold 4
	0 Precision: 0.52 Recall: 0.66 F1: 0.58 Accuracy: 0.57
	1 Precision: 0.72 Recall: 0.44 F1: 0.54 Accuracy: 0.70
	-1 Precision: 0.44 Recall: 0.46 F1: 0.45 Accuracy: 0.66
	Fold 5
	0 Precision: 0.60 Recall: 0.81 F1: 0.69 Accuracy: 0.65
	1 Precision: 0.56 Recall: 0.49 F1: 0.52 Accuracy: 0.78
	-1 Precision: 0.70 Recall: 0.44 F1: 0.54 Accuracy: 0.70
	Fold 6
	0 Precision: 0.69 Recall: 0.77 F1: 0.73 Accuracy: 0.69
	1 Precision: 0.59 Recall: 0.53 F1: 0.56 Accuracy: 0.72
	-1 Precision: 0.50 Recall: 0.43 F1: 0.47 Accuracy: 0.73
	Fold 7
	0 Precision: 0.57 Recall: 0.76 F1: 0.65 Accuracy: 0.62
	1 Precision: 0.61 Recall: 0.44 F1: 0.51 Accuracy: 0.69
	-1 Precision: 0.49 Recall: 0.38 F1: 0.43 Accuracy: 0.67
	Fold 8
	0 Precision: 0.66 Recall: 0.82 F1: 0.73 Accuracy: 0.70
	1 Precision: 0.56 Recall: 0.52 F1: 0.54 Accuracy: 0.70
	-1 Precision: 0.65 Recall: 0.41 F1: 0.50 Accuracy: 0.76
	Fold 9
	0 Precision: 0.65 Recall: 0.83 F1: 0.73 Accuracy: 0.67
	1 Precision: 0.70 Recall: 0.35 F1: 0.47 Accuracy: 0.75
	-1 Precision: 0.58 Recall: 0.57 F1: 0.57 Accuracy: 0.77

Cost: 0.20
	Fold 0
	0 Precision: 0.56 Recall: 0.79 F1: 0.66 Accuracy: 0.67
	1 Precision: 0.67 Recall: 0.55 F1: 0.60 Accuracy: 0.71
	-1 Precision: 0.65 Recall: 0.46 F1: 0.54 Accuracy: 0.73
	Fold 1
	0 Precision: 0.53 Recall: 0.79 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.69 Recall: 0.56 F1: 0.62 Accuracy: 0.75
	-1 Precision: 0.67 Recall: 0.43 F1: 0.52 Accuracy: 0.69
	Fold 2
	0 Precision: 0.54 Recall: 0.71 F1: 0.61 Accuracy: 0.60
	1 Precision: 0.56 Recall: 0.51 F1: 0.53 Accuracy: 0.68
	-1 Precision: 0.54 Recall: 0.36 F1: 0.43 Accuracy: 0.65
	Fold 3
	0 Precision: 0.57 Recall: 0.76 F1: 0.65 Accuracy: 0.64
	1 Precision: 0.69 Recall: 0.55 F1: 0.61 Accuracy: 0.71
	-1 Precision: 0.44 Recall: 0.34 F1: 0.38 Accuracy: 0.66
	Fold 4
	0 Precision: 0.57 Recall: 0.70 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.72 Recall: 0.48 F1: 0.58 Accuracy: 0.72
	-1 Precision: 0.48 Recall: 0.50 F1: 0.49 Accuracy: 0.69
	Fold 5
	0 Precision: 0.60 Recall: 0.78 F1: 0.68 Accuracy: 0.65
	1 Precision: 0.53 Recall: 0.51 F1: 0.52 Accuracy: 0.77
	-1 Precision: 0.67 Recall: 0.44 F1: 0.53 Accuracy: 0.69
	Fold 6
	0 Precision: 0.69 Recall: 0.70 F1: 0.70 Accuracy: 0.67
	1 Precision: 0.55 Recall: 0.54 F1: 0.55 Accuracy: 0.71
	-1 Precision: 0.50 Recall: 0.50 F1: 0.50 Accuracy: 0.73
	Fold 7
	0 Precision: 0.58 Recall: 0.71 F1: 0.64 Accuracy: 0.62
	1 Precision: 0.59 Recall: 0.44 F1: 0.50 Accuracy: 0.68
	-1 Precision: 0.47 Recall: 0.44 F1: 0.45 Accuracy: 0.66
	Fold 8
	0 Precision: 0.66 Recall: 0.80 F1: 0.72 Accuracy: 0.69
	1 Precision: 0.56 Recall: 0.50 F1: 0.53 Accuracy: 0.70
	-1 Precision: 0.60 Recall: 0.43 F1: 0.50 Accuracy: 0.75
	Fold 9
	0 Precision: 0.66 Recall: 0.84 F1: 0.74 Accuracy: 0.68
	1 Precision: 0.67 Recall: 0.33 F1: 0.44 Accuracy: 0.74
	-1 Precision: 0.57 Recall: 0.57 F1: 0.57 Accuracy: 0.76

Cost: 0.30
	Fold 0
	0 Precision: 0.56 Recall: 0.78 F1: 0.65 Accuracy: 0.67
	1 Precision: 0.66 Recall: 0.55 F1: 0.60 Accuracy: 0.70
	-1 Precision: 0.63 Recall: 0.46 F1: 0.53 Accuracy: 0.72
	Fold 1
	0 Precision: 0.54 Recall: 0.79 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.70 Recall: 0.56 F1: 0.62 Accuracy: 0.75
	-1 Precision: 0.64 Recall: 0.43 F1: 0.51 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.71 F1: 0.62 Accuracy: 0.61
	1 Precision: 0.56 Recall: 0.53 F1: 0.54 Accuracy: 0.69
	-1 Precision: 0.55 Recall: 0.38 F1: 0.45 Accuracy: 0.66
	Fold 3
	0 Precision: 0.58 Recall: 0.73 F1: 0.65 Accuracy: 0.65
	1 Precision: 0.67 Recall: 0.55 F1: 0.60 Accuracy: 0.70
	-1 Precision: 0.44 Recall: 0.38 F1: 0.41 Accuracy: 0.66
	Fold 4
	0 Precision: 0.58 Recall: 0.69 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.73 Recall: 0.50 F1: 0.59 Accuracy: 0.72
	-1 Precision: 0.46 Recall: 0.50 F1: 0.48 Accuracy: 0.68
	Fold 5
	0 Precision: 0.60 Recall: 0.76 F1: 0.67 Accuracy: 0.63
	1 Precision: 0.51 Recall: 0.51 F1: 0.51 Accuracy: 0.76
	-1 Precision: 0.64 Recall: 0.42 F1: 0.51 Accuracy: 0.67
	Fold 6
	0 Precision: 0.70 Recall: 0.69 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.54 F1: 0.55 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.48 F1: 0.46 Accuracy: 0.70
	Fold 7
	0 Precision: 0.59 Recall: 0.73 F1: 0.65 Accuracy: 0.63
	1 Precision: 0.60 Recall: 0.44 F1: 0.51 Accuracy: 0.68
	-1 Precision: 0.47 Recall: 0.44 F1: 0.45 Accuracy: 0.66
	Fold 8
	0 Precision: 0.67 Recall: 0.79 F1: 0.72 Accuracy: 0.69
	1 Precision: 0.56 Recall: 0.50 F1: 0.53 Accuracy: 0.70
	-1 Precision: 0.61 Recall: 0.47 F1: 0.53 Accuracy: 0.75
	Fold 9
	0 Precision: 0.66 Recall: 0.83 F1: 0.73 Accuracy: 0.68
	1 Precision: 0.67 Recall: 0.33 F1: 0.44 Accuracy: 0.74
	-1 Precision: 0.57 Recall: 0.59 F1: 0.58 Accuracy: 0.77

Cost: 0.40
	Fold 0
	0 Precision: 0.56 Recall: 0.76 F1: 0.65 Accuracy: 0.67
	1 Precision: 0.67 Recall: 0.56 F1: 0.61 Accuracy: 0.70
	-1 Precision: 0.62 Recall: 0.46 F1: 0.53 Accuracy: 0.72
	Fold 1
	0 Precision: 0.53 Recall: 0.79 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.72 Recall: 0.56 F1: 0.63 Accuracy: 0.75
	-1 Precision: 0.64 Recall: 0.43 F1: 0.51 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.71 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.56 Recall: 0.54 F1: 0.55 Accuracy: 0.69
	-1 Precision: 0.55 Recall: 0.38 F1: 0.45 Accuracy: 0.66
	Fold 3
	0 Precision: 0.57 Recall: 0.68 F1: 0.62 Accuracy: 0.63
	1 Precision: 0.63 Recall: 0.55 F1: 0.59 Accuracy: 0.68
	-1 Precision: 0.45 Recall: 0.40 F1: 0.42 Accuracy: 0.66
	Fold 4
	0 Precision: 0.61 Recall: 0.70 F1: 0.65 Accuracy: 0.65
	1 Precision: 0.74 Recall: 0.55 F1: 0.63 Accuracy: 0.75
	-1 Precision: 0.46 Recall: 0.50 F1: 0.48 Accuracy: 0.69
	Fold 5
	0 Precision: 0.60 Recall: 0.74 F1: 0.66 Accuracy: 0.63
	1 Precision: 0.48 Recall: 0.51 F1: 0.49 Accuracy: 0.74
	-1 Precision: 0.65 Recall: 0.42 F1: 0.51 Accuracy: 0.67
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.55 Recall: 0.56 F1: 0.56 Accuracy: 0.70
	-1 Precision: 0.46 Recall: 0.48 F1: 0.47 Accuracy: 0.71
	Fold 7
	0 Precision: 0.58 Recall: 0.70 F1: 0.64 Accuracy: 0.62
	1 Precision: 0.59 Recall: 0.44 F1: 0.50 Accuracy: 0.68
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.66
	Fold 8
	0 Precision: 0.67 Recall: 0.78 F1: 0.72 Accuracy: 0.70
	1 Precision: 0.57 Recall: 0.52 F1: 0.54 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.49 F1: 0.54 Accuracy: 0.75
	Fold 9
	0 Precision: 0.65 Recall: 0.81 F1: 0.72 Accuracy: 0.67
	1 Precision: 0.62 Recall: 0.33 F1: 0.43 Accuracy: 0.73
	-1 Precision: 0.57 Recall: 0.59 F1: 0.58 Accuracy: 0.76

Cost: 0.50
	Fold 0
	0 Precision: 0.56 Recall: 0.76 F1: 0.65 Accuracy: 0.67
	1 Precision: 0.68 Recall: 0.56 F1: 0.62 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.72
	Fold 1
	0 Precision: 0.53 Recall: 0.79 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.72 Recall: 0.56 F1: 0.63 Accuracy: 0.75
	-1 Precision: 0.64 Recall: 0.43 F1: 0.51 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.70 F1: 0.62 Accuracy: 0.62
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.69
	-1 Precision: 0.55 Recall: 0.38 F1: 0.45 Accuracy: 0.66
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.61 Accuracy: 0.63
	1 Precision: 0.63 Recall: 0.55 F1: 0.59 Accuracy: 0.68
	-1 Precision: 0.44 Recall: 0.40 F1: 0.42 Accuracy: 0.65
	Fold 4
	0 Precision: 0.61 Recall: 0.70 F1: 0.65 Accuracy: 0.65
	1 Precision: 0.73 Recall: 0.55 F1: 0.62 Accuracy: 0.74
	-1 Precision: 0.46 Recall: 0.50 F1: 0.48 Accuracy: 0.69
	Fold 5
	0 Precision: 0.60 Recall: 0.71 F1: 0.65 Accuracy: 0.63
	1 Precision: 0.48 Recall: 0.51 F1: 0.49 Accuracy: 0.74
	-1 Precision: 0.63 Recall: 0.45 F1: 0.52 Accuracy: 0.67
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.48 F1: 0.46 Accuracy: 0.70
	Fold 7
	0 Precision: 0.59 Recall: 0.69 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.57 Recall: 0.44 F1: 0.50 Accuracy: 0.67
	-1 Precision: 0.46 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.67 Recall: 0.77 F1: 0.71 Accuracy: 0.69
	1 Precision: 0.57 Recall: 0.52 F1: 0.54 Accuracy: 0.71
	-1 Precision: 0.59 Recall: 0.49 F1: 0.53 Accuracy: 0.75
	Fold 9
	0 Precision: 0.65 Recall: 0.80 F1: 0.72 Accuracy: 0.66
	1 Precision: 0.61 Recall: 0.35 F1: 0.45 Accuracy: 0.73
	-1 Precision: 0.57 Recall: 0.57 F1: 0.57 Accuracy: 0.76

Cost: 0.60
	Fold 0
	0 Precision: 0.56 Recall: 0.76 F1: 0.65 Accuracy: 0.67
	1 Precision: 0.68 Recall: 0.56 F1: 0.62 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.72
	Fold 1
	0 Precision: 0.53 Recall: 0.78 F1: 0.63 Accuracy: 0.64
	1 Precision: 0.69 Recall: 0.56 F1: 0.62 Accuracy: 0.74
	-1 Precision: 0.66 Recall: 0.43 F1: 0.52 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.70 F1: 0.62 Accuracy: 0.62
	1 Precision: 0.55 Recall: 0.56 F1: 0.56 Accuracy: 0.69
	-1 Precision: 0.56 Recall: 0.38 F1: 0.45 Accuracy: 0.67
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.61 Accuracy: 0.62
	1 Precision: 0.63 Recall: 0.54 F1: 0.58 Accuracy: 0.67
	-1 Precision: 0.43 Recall: 0.40 F1: 0.41 Accuracy: 0.65
	Fold 4
	0 Precision: 0.60 Recall: 0.69 F1: 0.64 Accuracy: 0.64
	1 Precision: 0.69 Recall: 0.55 F1: 0.61 Accuracy: 0.72
	-1 Precision: 0.45 Recall: 0.46 F1: 0.46 Accuracy: 0.68
	Fold 5
	0 Precision: 0.59 Recall: 0.70 F1: 0.64 Accuracy: 0.62
	1 Precision: 0.47 Recall: 0.51 F1: 0.49 Accuracy: 0.73
	-1 Precision: 0.63 Recall: 0.45 F1: 0.52 Accuracy: 0.66
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.48 F1: 0.46 Accuracy: 0.70
	Fold 7
	0 Precision: 0.58 Recall: 0.69 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.57 Recall: 0.44 F1: 0.50 Accuracy: 0.67
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.67 Recall: 0.77 F1: 0.72 Accuracy: 0.70
	1 Precision: 0.58 Recall: 0.52 F1: 0.55 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.53 F1: 0.57 Accuracy: 0.76
	Fold 9
	0 Precision: 0.66 Recall: 0.81 F1: 0.73 Accuracy: 0.67
	1 Precision: 0.61 Recall: 0.35 F1: 0.45 Accuracy: 0.73
	-1 Precision: 0.57 Recall: 0.57 F1: 0.57 Accuracy: 0.76

Cost: 0.70
	Fold 0
	0 Precision: 0.54 Recall: 0.76 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.68 Recall: 0.54 F1: 0.60 Accuracy: 0.70
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.71
	Fold 1
	0 Precision: 0.54 Recall: 0.75 F1: 0.63 Accuracy: 0.64
	1 Precision: 0.62 Recall: 0.56 F1: 0.59 Accuracy: 0.72
	-1 Precision: 0.67 Recall: 0.44 F1: 0.53 Accuracy: 0.69
	Fold 2
	0 Precision: 0.57 Recall: 0.70 F1: 0.63 Accuracy: 0.62
	1 Precision: 0.54 Recall: 0.54 F1: 0.54 Accuracy: 0.68
	-1 Precision: 0.56 Recall: 0.39 F1: 0.46 Accuracy: 0.67
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.62 Accuracy: 0.63
	1 Precision: 0.62 Recall: 0.52 F1: 0.57 Accuracy: 0.67
	-1 Precision: 0.43 Recall: 0.42 F1: 0.42 Accuracy: 0.65
	Fold 4
	0 Precision: 0.60 Recall: 0.69 F1: 0.64 Accuracy: 0.64
	1 Precision: 0.67 Recall: 0.55 F1: 0.60 Accuracy: 0.72
	-1 Precision: 0.47 Recall: 0.46 F1: 0.46 Accuracy: 0.69
	Fold 5
	0 Precision: 0.60 Recall: 0.70 F1: 0.65 Accuracy: 0.63
	1 Precision: 0.47 Recall: 0.51 F1: 0.49 Accuracy: 0.73
	-1 Precision: 0.63 Recall: 0.46 F1: 0.54 Accuracy: 0.67
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.57 Recall: 0.56 F1: 0.57 Accuracy: 0.71
	-1 Precision: 0.46 Recall: 0.50 F1: 0.48 Accuracy: 0.71
	Fold 7
	0 Precision: 0.57 Recall: 0.69 F1: 0.63 Accuracy: 0.61
	1 Precision: 0.57 Recall: 0.43 F1: 0.49 Accuracy: 0.66
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.67 Recall: 0.77 F1: 0.72 Accuracy: 0.70
	1 Precision: 0.58 Recall: 0.52 F1: 0.55 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.53 F1: 0.57 Accuracy: 0.76
	Fold 9
	0 Precision: 0.66 Recall: 0.80 F1: 0.72 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.33 F1: 0.42 Accuracy: 0.71
	-1 Precision: 0.59 Recall: 0.59 F1: 0.59 Accuracy: 0.77

Cost: 0.80
	Fold 0
	0 Precision: 0.54 Recall: 0.76 F1: 0.64 Accuracy: 0.65
	1 Precision: 0.68 Recall: 0.54 F1: 0.60 Accuracy: 0.70
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.71
	Fold 1
	0 Precision: 0.54 Recall: 0.75 F1: 0.63 Accuracy: 0.64
	1 Precision: 0.62 Recall: 0.56 F1: 0.59 Accuracy: 0.72
	-1 Precision: 0.67 Recall: 0.44 F1: 0.53 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.70 F1: 0.62 Accuracy: 0.62
	1 Precision: 0.55 Recall: 0.56 F1: 0.56 Accuracy: 0.69
	-1 Precision: 0.59 Recall: 0.39 F1: 0.47 Accuracy: 0.68
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.62 Accuracy: 0.63
	1 Precision: 0.62 Recall: 0.52 F1: 0.57 Accuracy: 0.67
	-1 Precision: 0.43 Recall: 0.42 F1: 0.42 Accuracy: 0.65
	Fold 4
	0 Precision: 0.60 Recall: 0.69 F1: 0.64 Accuracy: 0.64
	1 Precision: 0.69 Recall: 0.55 F1: 0.61 Accuracy: 0.72
	-1 Precision: 0.46 Recall: 0.46 F1: 0.46 Accuracy: 0.68
	Fold 5
	0 Precision: 0.60 Recall: 0.71 F1: 0.65 Accuracy: 0.63
	1 Precision: 0.47 Recall: 0.51 F1: 0.49 Accuracy: 0.74
	-1 Precision: 0.65 Recall: 0.46 F1: 0.54 Accuracy: 0.68
	Fold 6
	0 Precision: 0.69 Recall: 0.68 F1: 0.69 Accuracy: 0.66
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.70
	-1 Precision: 0.44 Recall: 0.46 F1: 0.45 Accuracy: 0.70
	Fold 7
	0 Precision: 0.56 Recall: 0.69 F1: 0.62 Accuracy: 0.60
	1 Precision: 0.57 Recall: 0.41 F1: 0.48 Accuracy: 0.66
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.68 Recall: 0.77 F1: 0.72 Accuracy: 0.70
	1 Precision: 0.57 Recall: 0.52 F1: 0.54 Accuracy: 0.71
	-1 Precision: 0.60 Recall: 0.53 F1: 0.57 Accuracy: 0.76
	Fold 9
	0 Precision: 0.65 Recall: 0.77 F1: 0.71 Accuracy: 0.66
	1 Precision: 0.58 Recall: 0.35 F1: 0.44 Accuracy: 0.72
	-1 Precision: 0.55 Recall: 0.59 F1: 0.57 Accuracy: 0.75

Cost: 0.90
	Fold 0
	0 Precision: 0.54 Recall: 0.75 F1: 0.63 Accuracy: 0.65
	1 Precision: 0.67 Recall: 0.54 F1: 0.59 Accuracy: 0.69
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.71
	Fold 1
	0 Precision: 0.54 Recall: 0.75 F1: 0.63 Accuracy: 0.64
	1 Precision: 0.62 Recall: 0.56 F1: 0.59 Accuracy: 0.72
	-1 Precision: 0.67 Recall: 0.44 F1: 0.53 Accuracy: 0.69
	Fold 2
	0 Precision: 0.57 Recall: 0.68 F1: 0.62 Accuracy: 0.62
	1 Precision: 0.55 Recall: 0.56 F1: 0.56 Accuracy: 0.69
	-1 Precision: 0.58 Recall: 0.41 F1: 0.48 Accuracy: 0.68
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.62 Accuracy: 0.63
	1 Precision: 0.62 Recall: 0.51 F1: 0.56 Accuracy: 0.67
	-1 Precision: 0.43 Recall: 0.43 F1: 0.43 Accuracy: 0.65
	Fold 4
	0 Precision: 0.59 Recall: 0.67 F1: 0.63 Accuracy: 0.63
	1 Precision: 0.66 Recall: 0.55 F1: 0.60 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.44 F1: 0.44 Accuracy: 0.68
	Fold 5
	0 Precision: 0.59 Recall: 0.70 F1: 0.64 Accuracy: 0.62
	1 Precision: 0.45 Recall: 0.49 F1: 0.47 Accuracy: 0.73
	-1 Precision: 0.63 Recall: 0.46 F1: 0.54 Accuracy: 0.67
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.48 F1: 0.46 Accuracy: 0.70
	Fold 7
	0 Precision: 0.56 Recall: 0.69 F1: 0.62 Accuracy: 0.60
	1 Precision: 0.57 Recall: 0.41 F1: 0.48 Accuracy: 0.66
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.69 Recall: 0.76 F1: 0.72 Accuracy: 0.71
	1 Precision: 0.58 Recall: 0.55 F1: 0.56 Accuracy: 0.72
	-1 Precision: 0.60 Recall: 0.53 F1: 0.57 Accuracy: 0.76
	Fold 9
	0 Precision: 0.65 Recall: 0.77 F1: 0.71 Accuracy: 0.66
	1 Precision: 0.58 Recall: 0.35 F1: 0.44 Accuracy: 0.72
	-1 Precision: 0.55 Recall: 0.59 F1: 0.57 Accuracy: 0.75

Cost: 1.00
	Fold 0
	0 Precision: 0.54 Recall: 0.74 F1: 0.62 Accuracy: 0.64
	1 Precision: 0.66 Recall: 0.54 F1: 0.59 Accuracy: 0.69
	-1 Precision: 0.60 Recall: 0.46 F1: 0.52 Accuracy: 0.71
	Fold 1
	0 Precision: 0.54 Recall: 0.75 F1: 0.63 Accuracy: 0.64
	1 Precision: 0.62 Recall: 0.56 F1: 0.59 Accuracy: 0.72
	-1 Precision: 0.67 Recall: 0.44 F1: 0.53 Accuracy: 0.69
	Fold 2
	0 Precision: 0.56 Recall: 0.67 F1: 0.61 Accuracy: 0.62
	1 Precision: 0.55 Recall: 0.56 F1: 0.56 Accuracy: 0.69
	-1 Precision: 0.57 Recall: 0.41 F1: 0.48 Accuracy: 0.67
	Fold 3
	0 Precision: 0.57 Recall: 0.67 F1: 0.62 Accuracy: 0.63
	1 Precision: 0.62 Recall: 0.51 F1: 0.56 Accuracy: 0.67
	-1 Precision: 0.43 Recall: 0.43 F1: 0.43 Accuracy: 0.65
	Fold 4
	0 Precision: 0.59 Recall: 0.67 F1: 0.63 Accuracy: 0.63
	1 Precision: 0.67 Recall: 0.56 F1: 0.61 Accuracy: 0.72
	-1 Precision: 0.46 Recall: 0.44 F1: 0.45 Accuracy: 0.68
	Fold 5
	0 Precision: 0.58 Recall: 0.69 F1: 0.63 Accuracy: 0.61
	1 Precision: 0.44 Recall: 0.49 F1: 0.46 Accuracy: 0.72
	-1 Precision: 0.63 Recall: 0.45 F1: 0.52 Accuracy: 0.66
	Fold 6
	0 Precision: 0.70 Recall: 0.68 F1: 0.69 Accuracy: 0.67
	1 Precision: 0.56 Recall: 0.56 F1: 0.56 Accuracy: 0.71
	-1 Precision: 0.45 Recall: 0.48 F1: 0.46 Accuracy: 0.70
	Fold 7
	0 Precision: 0.56 Recall: 0.69 F1: 0.62 Accuracy: 0.60
	1 Precision: 0.53 Recall: 0.38 F1: 0.44 Accuracy: 0.65
	-1 Precision: 0.47 Recall: 0.45 F1: 0.46 Accuracy: 0.65
	Fold 8
	0 Precision: 0.68 Recall: 0.75 F1: 0.71 Accuracy: 0.70
	1 Precision: 0.56 Recall: 0.53 F1: 0.55 Accuracy: 0.70
	-1 Precision: 0.60 Recall: 0.53 F1: 0.57 Accuracy: 0.76
	Fold 9
	0 Precision: 0.65 Recall: 0.77 F1: 0.71 Accuracy: 0.66
	1 Precision: 0.58 Recall: 0.35 F1: 0.44 Accuracy: 0.72
	-1 Precision: 0.55 Recall: 0.59 F1: 0.57 Accuracy: 0.75

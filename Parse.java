import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import java.io.*;


public class Parse {
    public static void main(String [] args) throws IOException{
        LexicalizedParser parser = LexicalizedParser.loadModel(args[0]);
        BufferedReader in = new BufferedReader(new FileReader(args[1]));
        PrintWriter out = new PrintWriter(args[2]);
        TreePrint tp = parser.getTreePrint();

        String line;
        Tree tree;
        while ((line = in.readLine()) != null){
            tree = parser.parse(line);
            tp.printTree(tree, out);
            out.print("\n");
        }
        out.close();
    }
}


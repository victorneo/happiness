from collections import defaultdict
import pickle
import os
import subprocess

from gensim import corpora, models, similarities
from gensim.utils import dict_from_corpus
from lxml import etree
from nltk import word_tokenize, NaiveBayesClassifier, ne_chunk
from sklearn.cross_validation import KFold
from sklearn.externals import joblib
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import LinearSVC

from pos_tagging import tag_tweets
from parsing import parse_tweets
from sentiment_evaluation import extract_features


TEST_SET_START = 7426
TEST_SET_END = 14999 + 1
CLF_MODEL = 'data/sentiment/clf_0.8.dat'
CLF_DICT = 'data/sentiment/dict_0.8.dat'
CLF_TFIDF = 'data/sentiment/tfidf_0.8.dat'
POS_TEST_PICKLE = 'data/sentiment/pos_test_tweets.dat'
PARSED_TEST_PICKLE = 'data/sentiment/parsed_test_tweets.dat'
RESULTS_FILE = 'data/sentiment/results.txt'


def classify_test_tweets():
    clf = joblib.load(CLF_MODEL)
    dictionary = pickle.load(open(CLF_DICT, 'r'))
    corpus_num_features = dictionary.keys()[0]
    tfidf = pickle.load(open(CLF_TFIDF, 'r'))
    tweets_text = []

    for i in range(TEST_SET_START, TEST_SET_END):
        tree = etree.parse('data/tweets/{0:05d}.xml'.format(i))
        tweets_text.append(tree.find('text').text)

    if not os.path.exists(POS_TEST_PICKLE):
        pos_tweets = tag_tweets(tweets_text)
    else:
        pos_tweets = pickle.load(open(POS_TEST_PICKLE, 'r'))

    if not os.path.exists(PARSED_TEST_PICKLE):
        parsed_tweets = parse_tweets(tweets_text)
    else:
        parsed_tweets = pickle.load(open(PARSED_TEST_PICKLE, 'r'))

    test_features = []
    for (i, tweet) in enumerate(tweets_text):
        test_features.append(extract_features(tweet, pos_tweets[i], parsed_tweets[i]))

    tfidf_test_features = []
    for tf in test_features:
        vect = tfidf[dictionary.doc2bow([str(f.lower()) for f in tf])]
        vect_dictionary = {v[0]: v[1] for v in vect}
        vect_tfidf = []
        for cnf in range(0, corpus_num_features):
            if cnf in vect_dictionary:
                vect_tfidf.append(vect_dictionary[cnf])
            else:
                vect_tfidf.append(0.0)
        tfidf_test_features.append(vect_tfidf)

    predictions = clf.predict(tfidf_test_features)
    with open(RESULTS_FILE, 'w') as f:
        f.write('\n'.join(predictions))


if __name__ == '__main__':
    classify_test_tweets()

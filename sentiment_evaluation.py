from collections import defaultdict
import pickle
import os
import subprocess

from gensim import corpora, models, similarities
from gensim.utils import dict_from_corpus
from lxml import etree
from nltk import word_tokenize, NaiveBayesClassifier
from sklearn.cross_validation import KFold
from sklearn.externals import joblib
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import LinearSVC

from pos_tagging import tag_tweets
from parsing import parse_tweets

from pos_tagging import tag_tweets


LABELED_SET_START = 5426
LABELED_SET_END = 7425 + 1
LABELED_SET_PICKLE = 'data/sentiment/sentiment_tweets.dat'
FOLDS_PICKLE = 'data/sentiment/folds.dat'
POS_PICKLE = 'data/sentiment/pos.dat'
PARSED_PICKLE = 'data/sentiment/parsed.dat'
CURR_MODEL = 'data/sentiment/clf_%.1f.dat'
CURR_DICT = 'data/sentiment/dict_%.1f.dat'
CURR_TFIDF = 'data/sentiment/tfidf_%.1f.dat'

TWEET_INDEX = 0
POS_INDEX = 1
PARSED_INDEX = 2
POS_FEATURES = {'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS', 'SYM', 'UH'}
PARSED_FEATURES = {'ADJP', 'NP',}


def extract_features(tweet):
    # Assume tweet is a tuple of (text, pos tags, parser tags)
    # bag-of-words model
    feature = set()

    # Use (word + POS tag) as a feature for word sense disembiguation
    # if there is a word with different POS tag from parser vs Pos tagger
    # we add both

    for st in tweet[PARSED_INDEX].subtrees():
        if st.node in PARSED_FEATURES:
            for sst in st.subtrees():
                if sst.node in POS_FEATURES:
                    for leaf in sst.leaves():
                        feature.add(leaf)

    for tags in tweet[POS_INDEX]:
        if tags[1] in POS_FEATURES:
            feature.add(tags[0])

    # Feature for tweets without any features
    if len(feature) == 0:
        feature.add('NoFeature')

    return list(feature)


def classify():
    if not os.path.exists('data/sentiment'):
        os.makedirs('data/sentiment')

    # Load labeled tweets
    if not os.path.exists(LABELED_SET_PICKLE):
        # Retrieve sentiment class and tweet text from labeled set
        tweets = []
        classes = []
        for i in range(LABELED_SET_START, LABELED_SET_END):
            tree = etree.parse('data/tweets/{0:05d}.xml'.format(i))

            s_class = int(tree.find('sentiment').text)
            text = tree.find('text').text
            classes.append(s_class)
            tweets.append(text)

        pickle.dump((classes, tweets), open(LABELED_SET_PICKLE, 'w'))
    else:
        classes, tweets = pickle.load(open(LABELED_SET_PICKLE, 'r'))

    if not os.path.exists(POS_PICKLE):
        pos_tweets = tag_tweets(tweets)
        pickle.dump(pos_tweets, open(POS_PICKLE, 'w'))
    else:
        pos_tweets = pickle.load(open(POS_PICKLE, 'r'))

    if not os.path.exists(PARSED_PICKLE):
        parsed_tweets = parse_tweets(tweets)
        pickle.dump(parsed_tweets, open(PARSED_PICKLE, 'w'))
    else:
        parsed_tweets = pickle.load(open(PARSED_PICKLE, 'r'))

    if not os.path.exists(FOLDS_PICKLE):
        # Generate 10 sets of data, for use with 10-fold cross validation
        folds = []
        kf = KFold(len(classes), n_folds=10, indices=False)
        for train, test in kf:
            train_set = []
            train_set_classes = []
            test_set = []
            test_set_classes = []
            for (i, is_train) in enumerate(train):
                if is_train:
                    train_set.append((tweets[i], pos_tweets[i], parsed_tweets[i]))
                    train_set_classes.append(classes[i])
                else:
                    test_set.append((tweets[i], pos_tweets[i], parsed_tweets[i]))
                    test_set_classes.append(classes[i])

            folds.append((train_set_classes, train_set, test_set_classes, test_set))

        pickle.dump(folds, open(FOLDS_PICKLE, 'w'))
    else:
        folds = pickle.load(open(FOLDS_PICKLE, 'r'))

    # Possible features to use: Unigrams, tf-idf weights
    # Use Parser + POS tagger to identify Adjectives, Nouns etc

    # Let's try unigrams first
    for cost in range(1, 11):
        C = float(cost) / 10
        print '\nCost: %.2f' % (C)
        for (i, fold) in enumerate(folds):
            train_set_classes = fold[0]
            train_set = fold[1]
            test_set_classes = fold[2]
            test_set = fold[3]
            individual_features = []

            all_features = []
            train_features = []
            for (j, t) in enumerate(train_set):
                feature = extract_features(t)
                all_features = all_features + feature
                individual_features.append([f.lower() for f in feature])
                train_features.append((feature, train_set_classes[j]))

            all_features = set(all_features)

            # generate binary features for training set
            #binary_train_features = []
            #for tf in train_features:
            #    feature, train_class = tf[0], tf[1]
            #    binary_feature = []
            #    for f in all_features:
            #        if f in feature:
            #            binary_feature.append(1)
            #        else:
            #            binary_feature.append(0)

            #    binary_train_features.append(binary_feature)

            # generate binary features for testing set
            #binary_test_features = []
            test_features = []
            for (j, t) in enumerate(test_set):
                feature = extract_features(t)
                test_features.append(feature)
            #    binary_feature = []

            #    for f in all_features:
            #        if f in feature:
            #            binary_feature.append(1)
            #        else:
            #            binary_feature.append(0)

            #    binary_test_features.append(binary_feature)

            # Use gensim library to build tf-idf models
            dictionary = corpora.Dictionary(individual_features)
            corpus = []

            # build corpus of vector representation
            tfidf_train_features = []
            for tf in train_features:
                feature, train_class = tf[0], tf[1]
                corpus.append(dictionary.doc2bow([f.lower() for f in feature]))

            # generate tf-idf features for training and test set
            tfidf_train_features = []
            tfidf_test_features = []
            corpus_num_features = dict_from_corpus(corpus).keys()[0]

            tfidf = models.TfidfModel(corpus)
            for tf in train_features:
                vect_dictionary = {v[0]: v[1] for v in tfidf[dictionary.doc2bow([f.lower() for f in tf[0]])]}
                vect_tfidf = []
                for cnf in range(0, corpus_num_features):
                    if cnf in vect_dictionary:
                        vect_tfidf.append(vect_dictionary[cnf])
                    else:
                        vect_tfidf.append(0.0)
                tfidf_train_features.append(vect_tfidf)

            for tf in test_features:
                vect = tfidf[dictionary.doc2bow([str(f.lower()) for f in tf])]
                vect_dictionary = {v[0]: v[1] for v in vect}
                vect_tfidf = []
                for cnf in range(0, corpus_num_features):
                    if cnf in vect_dictionary:
                        vect_tfidf.append(vect_dictionary[cnf])
                    else:
                        vect_tfidf.append(0.0)
                tfidf_test_features.append(vect_tfidf)

            # Create classifier
            clf = LinearSVC(C=C)
            #clf.fit(binary_train_features, train_set_classes)
            #predictions = clf.predict(binary_test_features)
            clf.fit(tfidf_train_features, train_set_classes)
            predictions = clf.predict(tfidf_test_features)

            class_counts = {k: defaultdict(int) for k in set(train_set_classes)}

            for (j, p) in enumerate(predictions):
                if p == test_set_classes[j]:
                    class_counts[p]['tp'] += 1
                    for k in [k for k in class_counts.keys() if k != p]:
                        class_counts[k]['tn'] += 1
                else:
                    class_counts[p]['fp'] += 1
                    class_counts[test_set_classes[j]]['fn'] += 1


            # Save classifier model with entire training data
            clf = LinearSVC(C=C)
            train_test_features = tfidf_train_features + tfidf_test_features
            train_test_classes = train_set_classes + test_set_classes
            clf.fit(train_test_features, train_test_classes)
            joblib.dump(clf, CURR_MODEL % (C), compress=9)
            pickle.dump(dictionary, open(CURR_DICT % (C), 'w'))
            pickle.dump(tfidf, open(CURR_TFIDF % (C), 'w'))

         #tp, fp, fn, tn = 0.0,0.0,0.0,0.0
        #if p == new_test_set_classes[j]:
        #    if p == 0:
        #   tp += 1
        #    else:
        #   tn += 1
        #else:
        #    if p == 0:
        #   fp += 1
            #    else:
        #   fn += 1

        #if tp > 0:
        #    precision = tp / (tp+fp)
        #    recall = tp / (tp+fn)
        #    f1 = 2 * precision * recall / (precision+recall)
        #else:
        #     precision, recall, f1 = 0.0, 0.0, 0.0
        #print 'Precision: %.2f, Recall: %.2f, F1: %.2f' % (precision, recall, f1)

            print '\tFold %i' % i

            for k in class_counts.keys():
                accuracy = 0.0
                precision = 0.0
                recall = 0.0
                f1 = 0.0
                if class_counts[k]['tp'] > 0:
                    precision = float(class_counts[k]['tp']) / (class_counts[k]['tp'] + class_counts[k]['fp'])
                    recall = float(class_counts[k]['tp']) / (class_counts[k]['tp'] + class_counts[k]['fn'])

                if class_counts[k]['tp'] > 0 and class_counts[k]['tn'] > 0:
                    accuracy = (float(class_counts[k]['tp']) + class_counts[k]['tn']) / (class_counts[k]['tp'] + class_counts[k]['tn'] + class_counts[k]['fn'] + class_counts[k]['fp'])

                if precision > 0 and recall > 0:
                    f1 = 2 * precision * recall / (precision + recall)
                print '\t', k, 'Precision: %.2f' % precision, 'Recall: %.2f' % recall, 'F1: %.2f' % f1, 'Accuracy: %.2f' % accuracy


if __name__ == '__main__':
    classify()
